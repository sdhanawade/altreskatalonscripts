package automationEr;

import java.io.File;
import java.io.IOException;
import java.util.Properties;

import org.openqa.selenium.WebDriver;


public class ErSetup {
	static File src = null;
	GetValues gobj = new GetValues();
	
	public static Properties object;
	
	public WebDriver erSetup (WebDriver driver) throws IOException{
		src = new File(getClass().getClassLoader().getResource("excelData/ErSetup.xlsx").getPath());
		gobj.getValues(src, driver);
		return driver;
	}

	public WebDriver erSetupError (WebDriver driver) throws IOException{
    src = new File(getClass().getClassLoader().getResource("excelData/ErSetupError.xlsx").getPath());
    gobj.getValues(src, driver);
    return driver;
  }
}
