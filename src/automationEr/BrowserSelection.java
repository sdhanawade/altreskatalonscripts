package automationEr;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

public class BrowserSelection {

	public static WebDriver driver;

	public WebDriver openChromeBrowser() {
		System.setProperty("webdriver.chrome.driver",getClass().getClassLoader().getResource("automationDriver/chromedriver.exe").getPath());
		driver = new ChromeDriver();
		return driver;
	}
	
	public WebDriver openFirefoxBrowser(){
		System.setProperty("webdriver.gecko.driver",getClass().getClassLoader().getResource("automationDriver/geckodriver.exe").getPath());
		driver = new FirefoxDriver();
		return driver;
	}
	
	public WebDriver openIEBrowser(){
		System.setProperty("webdriver.ie.driver",getClass().getClassLoader().getResource("automationDriver/IEDriverServer.exe").getPath());
		driver = new InternetExplorerDriver();
		return driver;
	}
	
}
