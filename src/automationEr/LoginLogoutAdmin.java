package automationEr;

import java.io.File;
import java.io.IOException;

import org.openqa.selenium.WebDriver;

public class LoginLogoutAdmin {
  static File src = null;
  GetValues gobj = new GetValues();

  public WebDriver loginToHRSymhony(WebDriver driver) throws IOException {
    src = new File(getClass().getClassLoader().getResource("excelData/LoginAdmin.xlsx").getPath());
    gobj.getValues(src, driver);
    return driver;
  }

  public WebDriver logoutToHRSymhony(WebDriver driver) throws Exception {
    src = new File(getClass().getClassLoader().getResource("excelData/LogoutAdmin.xlsx").getPath());
    gobj.getValues(src, driver);
    driver.quit();
   return driver;
  }
}
