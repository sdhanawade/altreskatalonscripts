package automationEr;

import java.util.List;
import java.util.Set;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

public class ElementCall {

  public void xpath(String value, String action, String input, WebDriver driver) {
    if (action.equals("click")) {
      driver.findElement(By.xpath(value)).click();
    } else if (action.equals("clear")) {
      driver.findElement(By.xpath(value)).clear();
    } else if (action.equals("sendKeys")) {
      if (input.contains(".")) {
        input = input.substring(0, input.indexOf("."));
      }
      driver.findElement(By.xpath(value)).sendKeys(input);
    }
  }

  public void action(String value, String type, String action, String input, WebDriver driver) {
    WebElement element;
    if (value.equals("xpath")) {
      element = driver.findElement(By.xpath(input));
      Actions actions = new Actions(driver);
      actions.moveToElement(element).click().perform();
    } else if (value.equals("id")) {
      element = driver.findElement(By.id(input));
      Actions actions = new Actions(driver);
      actions.moveToElement(element).click().perform();
    }

  }

  public void cssClick(String value, String action, WebDriver driver) {
    if (action.equals("click")) {
      driver.findElement(By.cssSelector(value)).click();
    }
  }

  public void className(String value, String action, WebDriver driver) {
    if (action.equals("click")) {
      driver.findElement(By.className(value)).click();
    }
  }

  public void id(String value, String action, String input, WebDriver driver) {
    if (action.equals("click")) {
      driver.findElement(By.id(value)).click();
    } else if (action.equals("clear")) {
      driver.findElement(By.id(value)).clear();
    } else if (action.equals("sendKeys")) {
      driver.findElement(By.id(value)).sendKeys(input);
    }
  }

  public void linkTextClick(String value, String action, WebDriver driver) {
    if (action.equals("click")) {
      if (value.contains(".")) {
        value = value.substring(0, value.indexOf("."));
      }
      driver.findElement(By.linkText(value)).click();
    }
  }

  public void name(String value, String action, String input, WebDriver driver) {
    if (action.equals("clear")) {
      driver.findElement(By.name(value)).clear();
    } else if (action.equals("sendKeys")) {
      if (value.contains(".")) {
        value = value.substring(0, value.indexOf("."));
      }
      driver.findElement(By.name(value)).sendKeys(input);
    } else if (action.equals("click")) {
      driver.findElement(By.name(value)).click();
    }
  }

  public void switchTo(String value, String type, String action, String input, WebDriver driver) {
    if (type.equals("frame")) {
      driver.switchTo().frame(value);
    } else if (type.equals("frame") && value.equals("id")) {
      driver.switchTo().frame(input);
    } else if (type.equals("window")) {
      if (value.equals("null")) {
        Set<String> value1 = driver.getWindowHandles();
        for (String value2 : value1) {
          value = value2;
          break;
        }
        driver.switchTo().window(value);
      } else {
        driver.switchTo().window(value);
      }
    } else if (type.equals("defaultContent")) {
      driver.switchTo().defaultContent();
    }
  }

  public void selectDropdown(String value, String action, String input, WebDriver driver) {
    if (value.equals("id")) {
      Select dropdown = new Select(driver.findElement(By.id(input)));
      if (action.contains(".")) {
        action = action.substring(0, action.indexOf("."));
      }
      dropdown.selectByVisibleText(String.valueOf(action));
    } else if (value.equals("xpath")) {
      Select dropdown = new Select(driver.findElement(By.xpath(input)));
      if (action.contains(".")) {
        action = action.substring(0, action.indexOf("."));
      }
      dropdown.selectByVisibleText(String.valueOf(action));
    } else if (value.equals("name")) {
      Select dropdown = new Select(driver.findElement(By.name(input)));
      if (action.contains(".")) {
        action = action.substring(0, action.indexOf("."));
      }
      dropdown.selectByVisibleText(String.valueOf(action));
    }

  }

  public void close(WebDriver driver) {
    driver.close();
  }

  public void quit(WebDriver driver) {
    driver.quit();
  }

  public void get(String value, WebDriver driver) {
    driver.get(value);
  }

  public void wait(WebDriver driver) {
    try {
      Thread.sleep(3000);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }

  public void maximise(WebDriver driver) {
    driver.manage().window().maximize();
  }

  public void alert(String value, String type, WebDriver driver) {
    if (type.equals("alert") && value.equals("accept")) {
      Alert alert = driver.switchTo().alert();
      alert.accept();
    } else {
      Alert alert = driver.switchTo().alert();
      alert.dismiss();
    }
  }

  public void getChildCount(String value, String type, WebDriver driver) {
    if (type.equals("getChildCount")) {
      List<WebElement> rows = driver.findElements(By.xpath(value));
   
      for (int rnum = 1; rnum < rows.size(); rnum++) {
       System.out.println("No of tr are : " + rows.size());
        //To locate that specific td list.
        List < WebElement > Specific_td = rows.get(rnum).findElements(By.xpath("//tbody[@id='processors']/tr/td[@class='first']//self::a"));
        System.out.println("Content text is : " + rows.get(rnum).getText() +  " size of cell: "+Specific_td.size());
       //click on specific td
        Specific_td.get(1).click();
        Alert alert = driver.switchTo().alert();
        alert.accept();
      }

    }

  }

  public void error(String input, WebDriver driver) {
    String actualText = driver.findElement(By.xpath("//div[@id='notices']/div")).getText()
        .replace(driver.findElement(By.xpath("//a[contains(text(),'close')]")).getText(), "").trim();

    if (input.equals(actualText)) {
      System.out.println("Pass");
    } else {
      System.out.println("Fail");
    }

  }

}
