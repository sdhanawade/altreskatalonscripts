package automationEr;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelData {
	private static XSSFSheet ExcelWSheet;
	private static XSSFWorkbook	ExcelWBook;
	private static XSSFCell Cell;
	private static XSSFRow Row;
	/*static File src = new File("");
	static File Payrollsrc = new File("");*/
	
	public static String cellToString(XSSFCell cell){
		CellType type = cell.getCellType();
		Object result;
		
		switch (type){
		case NUMERIC:
		/*	result = cell.getNumericCellValue();*/
			result = (int) Math.round(cell.getNumericCellValue());
			break;
		case STRING:
			result = cell.getStringCellValue();
			break;
		default:
			throw new RuntimeException("Invalid cell type." + cell.getCellType());
		}
		return result.toString();
	}
	
	public String getCellData(int RowNum, int ColNum){
		try{
			Cell = ExcelWSheet.getRow(RowNum).getCell(ColNum);
			String CellData = Cell.getStringCellValue();
			return CellData;
		}catch (Exception e){
			return "";
		}
	}
	
	public void setCellData(File Sheetname, String Result, int RowNum, int ColNum){
		try{
			FileInputStream fis = new FileInputStream(Sheetname);
			XSSFWorkbook wb = new XSSFWorkbook(fis);
			XSSFSheet sh1 = wb.getSheetAt(0);
			sh1.getRow(RowNum).createCell(ColNum).setCellValue(Result);
			FileOutputStream fout = new FileOutputStream(Sheetname);
			wb.write(fout);
			fout.close();
			wb.close();
		}catch(Exception e){
			System.out.println(e.getMessage());
		}
	}
	
	public List<List<String>> readValues(File Sheetname) throws IOException{
		List<List<String>> listOfLists = new ArrayList<List<String>>();
		String value = null;
		FileInputStream fis = new FileInputStream(Sheetname);
		ExcelWBook = new XSSFWorkbook(fis);
		XSSFSheet sh = ExcelWBook.getSheetAt(0);
		int rowsize = sh.getPhysicalNumberOfRows(); // sh.getLastRowNum() + 1;
		int columnsize = sh.getRow(0).getPhysicalNumberOfCells(); // sh.getRow(0).getLastCellNum();
		for (int i = 1; i < rowsize; i++) {
			List<String> singleList = new ArrayList<String>();
			Row = sh.getRow(i);
			for (int j = 0; j < columnsize; j++) {
				Cell = Row.getCell(j);
				if (Cell != null && Cell.getCellType() != CellType.BLANK) {
					value = cellToString(Cell);
					singleList.add(value);
				}
			}
			listOfLists.add(i - 1, singleList);
		}
		return listOfLists;
	}
	
}
