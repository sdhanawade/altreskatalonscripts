package automationEr;

import java.util.ArrayList;
import java.util.List;
import java.io.File;
import java.io.IOException;

import org.openqa.selenium.WebDriver;

public class GetValues {
	
	ExcelData obj = new ExcelData();
	ElementCall ecobj = new ElementCall();
	
	public void getValues(File SheetName, WebDriver driver) throws IOException {
		List<List<String>> listOfLists = new ArrayList<List<String>>();
		listOfLists = obj.readValues(SheetName);
		List<String> singleList = new ArrayList<String>();
		for(int i=0; i < listOfLists.size(); i++){
		singleList = listOfLists.get(i);
		switch (singleList.get(1)){
		case "get":
			ecobj.get(singleList.get(0), driver);
			break;
		case "xpath":
			ecobj.xpath(singleList.get(0), singleList.get(2), singleList.get(3), driver);
			break;
		case "close":
			ecobj.close(driver);
			break;
		case "wait":
				ecobj.wait(driver);
				break;
		case "maximise":
				ecobj.maximise(driver);
				break;
		case "select":
				ecobj.selectDropdown(singleList.get(0), singleList.get(2), singleList.get(3), driver);
				break;
		case "frame":
			ecobj.switchTo(singleList.get(0), singleList.get(1), singleList.get(2), singleList.get(3), driver);
			break;
		
		case "id":
			ecobj.id(singleList.get(0), singleList.get(2), singleList.get(3), driver);
			break;
		case "name":
			ecobj.name(singleList.get(0), singleList.get(2), singleList.get(3), driver);
			break;
		case "getChildCount":
			ecobj.getChildCount(singleList.get(0),singleList.get(1),driver);
			break;
		case "compareError":
			ecobj.error(singleList.get(0), driver);
			break;
		case "alert":
      ecobj.alert(singleList.get(0), singleList.get(1), driver);
      break;
		}
		}
	}
}
