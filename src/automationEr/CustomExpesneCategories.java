package automationEr;

import java.io.File;
import java.io.IOException;

import org.openqa.selenium.WebDriver;

public class CustomExpesneCategories {
	static File src = null;
	GetValues gobj = new GetValues();
	
	public WebDriver erCustomCategories(WebDriver driver) throws IOException {
		src = new File(getClass().getClassLoader().getResource("excelData/ErAddCustomeCategories.xlsx").getPath());
		gobj.getValues(src, driver);
		return null;
	}
	public WebDriver erCustomCategoriesErrorNotices(WebDriver driver) throws IOException {
    src = new File(getClass().getClassLoader().getResource("excelData/ErCustomeCategoriesError.xlsx").getPath());
    gobj.getValues(src, driver);
    return null;
  }
}
