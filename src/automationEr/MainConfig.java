package automationEr;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class MainConfig {
  static BrowserSelection bsobj = new BrowserSelection();
  static LoginLogoutEmployee llobj = new LoginLogoutEmployee();
  static LoginLogoutAdmin llaobj = new LoginLogoutAdmin();
  static ErSetup ersetupobj = new ErSetup();
  static ErSetup ersetuperrorobj = new ErSetup();
  static CustomExpesneCategories customecategories = new CustomExpesneCategories();
  static CustomExpesneCategories customecategorieserrorobj = new CustomExpesneCategories();
  static ErApprovalProcess approvalprocessobj = new ErApprovalProcess();
  static ErApprovalProcess approvalprocesserrorobj = new ErApprovalProcess();
  static ExcelData obj = new ExcelData();
  static String Result = "Pass";
  static String msg = null;
  private static final String TASKLIST = "tasklist";
  private static final String KILL = "taskkill /F /IM ";
  

  public static boolean isProcessRunning() throws Exception {

    Process p = Runtime.getRuntime().exec(TASKLIST);
    BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
    String line;
    while ((line = reader.readLine()) != null) {
      
     // System.out.println(line);
      if (line.contains("chromedriver.exe || geckodriver.exe ")) {
        return true;
      }
    }

    return false;

  }

  public static void killProcess(String serviceName) throws Exception {

    Runtime.getRuntime().exec(KILL + serviceName);

  }

  public static void main(String[] args) throws Exception {
    InputStream is = null;
    Properties prop = new Properties();
    try {
      is = MainConfig.class.getClassLoader().getResourceAsStream("automationEr//object.properties");
      // Load properties file
      prop.load(is);
    } catch (IOException e) {
      e.printStackTrace();
    }

    File src = new File(prop.getProperty("locationAllTestCaseFile"));
    int status = Integer.parseInt(prop.getProperty("statusColumn"));
    int comment = Integer.parseInt(prop.getProperty("errorColumn"));
    int module = Integer.parseInt(prop.getProperty("moduleColumn"));
    int scenario = Integer.parseInt(prop.getProperty("scenarioColumn"));

    List<List<String>> edata = new ArrayList<List<String>>();
    edata = obj.readValues(src);
    List<String> irow = new ArrayList<String>();
    for (int i = 0; i < edata.size(); i++) {
      irow = edata.get(i);

      switch (irow.get(module)) {
    /*  case "LoginLogoutEmployee": loginLogoutEmployee(irow.get(scenario)); 
       break;*/
       
       /*case "LoginLogoutAdmin": loginLogoutAdmin(irow.get(scenario)); 
       break;*/
       
      
      case "ErSetup": erSetup(irow.get(scenario)); 
       break;
   
     /*case "CustomExpesneCategories":customExpesneCategories(irow.get(scenario));
       break;*/
     
     /*case "ErApprovalProcess":erApprovalProcess(irow.get(scenario));
       break;*/
 }

      if (Result.equals("Pass")) {
        obj.setCellData(src, Result, i + 1, status);
      } else if (Result.equals("Fail")) {
        obj.setCellData(src, Result, i + 1, status);
        if (msg.contains("(S")) {
          msg = msg.substring(0, msg.indexOf("(S"));
        }
        obj.setCellData(src, msg, i + 1, comment);
      } else if (Result.equals("Not Executed")) {
        obj.setCellData(src, Result, i + 1, status);
      }

    }
    //check if driver is running , it get closed 
    String serviceName = null;
    if (isProcessRunning()) {

      killProcess(serviceName);
     }

  }

  public static void loginLogoutEmployee(String TestCase) {
    if (TestCase.equals("Validate employee is able to login to HR Symphony portal")) {
      try {
        llobj.logoutToHRSymhony(llobj.loginToHRSymhony(bsobj.openFirefoxBrowser()));
        Result = "Pass";
      } catch (Exception e) {
        msg = e.getMessage();
        Result = "Fail";
      }
    }
  }

  public static void loginLogoutAdmin(String TestCase) {
    if (TestCase.equals("Validate admin is able to login to HR Symphony portal")) {
      try {
        llaobj.logoutToHRSymhony(llaobj.loginToHRSymhony(bsobj.openFirefoxBrowser()));
        Result = "Pass";
      } catch (Exception e) {
        msg = e.getMessage();
        Result = "Fail";
      }
    }
  }

  public static void erSetup(String TestCase) {
   if (TestCase.equals("Validate ER setup under setting")) {
      try {
        ersetupobj.erSetup(llaobj.loginToHRSymhony(bsobj.openChromeBrowser()));
        Result = "Pass";
      } catch (Exception e) {
        msg = e.getMessage();
        Result = "Fail";
      }
    }
  /*if (TestCase.equals("Cheking ER setup Error Notice under setting")) {
      try {
        ersetuperrorobj.erSetupError(llaobj.loginToHRSymhony(bsobj.openChromeBrowser()));
        Result = "Pass";
      } catch (Exception e) {
        msg = e.getMessage();
        Result = "Fail";
      }
    }*/
  }
  
  
    
 
  public static void customExpesneCategories(String TestCase) {
   if (TestCase.equals("Add custom expense categories in setting")) {

      try {
        customecategories.erCustomCategories(llaobj.loginToHRSymhony(bsobj.openFirefoxBrowser()));
        Result = "Pass";
      } catch (Exception e) {
        msg = e.getMessage();
        Result = "Fail";
      }
    }
    if (TestCase.equals("Checking Error Notices for custom expense categories")) {

      try {
        customecategorieserrorobj.erCustomCategoriesErrorNotices(llaobj.loginToHRSymhony(bsobj.openFirefoxBrowser()));
        Result = "Pass";
      } catch (Exception e) {
        msg = e.getMessage();
        Result = "Fail";
      }
    }
  }
  
public static void erApprovalProcess(String TestCase){
 if (TestCase.equals("Add new Aprroval Process")) {

   try {
     llaobj.logoutToHRSymhony(approvalprocessobj.erApprovalProcess(llaobj.loginToHRSymhony(bsobj.openChromeBrowser())));
     Result = "Pass";
   } catch (Exception e) {
     msg = e.getMessage();
    Result = "Fail";
    }
  }
if (TestCase.equals("Checking Error Notices for Aprroval Process")) {

    try {
      approvalprocesserrorobj.erApprovalProcessError(llaobj.loginToHRSymhony(bsobj.openFirefoxBrowser()));
      Result = "Pass";
    } catch (Exception e) {
      msg = e.getMessage();
      Result = "Fail";
    }
  }
  
  }
  
  
}
