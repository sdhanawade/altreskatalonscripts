package automationEr;

import java.io.File;
import java.io.IOException;

import org.openqa.selenium.WebDriver;

public class ErApprovalProcess {
	static File src = null;
	GetValues gobj = new GetValues();
	
	public WebDriver erApprovalProcess (WebDriver driver) throws IOException {
		src = new File(getClass().getClassLoader().getResource("excelData/ErApprovalProcess.xlsx").getPath());
		gobj.getValues(src, driver);
		return null;
	}
	
	public WebDriver erApprovalProcessError (WebDriver driver) throws IOException {
    src = new File(getClass().getClassLoader().getResource("excelData/ErApprovalProcessError.xlsx").getPath());
    gobj.getValues(src, driver);
    return null;
  }

}
